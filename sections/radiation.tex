This section summarizes total ionizing dose (TID) and single event effect (SEE) tests and results for several design stages of the Nevis ADC. 

\subsection{TID tests with Nevis10}
To test the radiation tolerance of this ADC design, five chips were taken to the Massachusetts General Hospital Francis H. Burr Proton Therapy Center \cite{Kuppambatti:1566318}. Two of these (chips 1 and 2) had been fully characterized and showed excellent performance. The other three were selected using a socketed board (Figure \ref{fig:photosetup}). This board provided power and a clock signal to the ADC. To confirm that the chips operated as expected, a pure sine wave signal was given as input ($2 V_{p-p}$) and the analog residue was observed on an oscilloscope.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/radiation/nevis10_socket_scaled.png}
\end{center}
\caption{Test board for irradiation.}
\label{fig:photosetup}
\end{figure}

\begin{table}[!htb]
%\begin{scriptsize}
\begin{center}
\begin{tabular}{|cccccc|}
\hline
Chip Number & proton/cm\textsuperscript{2} & MRad & SNDR [dBc] & SFDR [dBc] & ENOB \\
& & & Pre/Post-Irradiation & Pre/Post-Irradiation & Pre/Post-Irradiation \\ 
\hline
1 & $1.01\times10^{14}$ & 5.33 & 67.85/67.78 & 73.66/77.8 & 10.98/10.97 \\
2 & $2.01\times10^{14}$ & 10.70 & 67.54/67.69 & 73.30/72.98 & 10.93/10.95 \\
\hline
\end{tabular}
\caption{Measurements of ADC performance before and after irradiation in a 227 MeV proton beam at $f_{\textrm{in}}=10$~MHz.}
\label{tab:nevis10results}
\end{center}
%\end{scriptsize}
\end{table}

To replicate the high radiation flux in the ATLAS detector, four of the chips were irradiated using the 227 MeV proton beam. During the irradiation, the chips were powered and a clock signal was applied, while the current drawn by the chip was monitored. Table \ref{tab:nevis10results} lists the dose for the two chips which received the largest dose. No further measurements were done using the three other chips which received smaller doses, given the performance of the high dose chips. In Figure \ref{fig:current}, the current over the course of one test is seen to be relatively constant, between 0.13 and 0.14 A. The drop observed at about 900 seconds shows the beam turn on. The slight rise and fall of the current which follows is due to the heating of the chip as it is irradiated.
In order to confirm the operation of the ADC immediately after irradiation, a pure sine wave signal was again applied to the ADC input and the analog residue was observed on an oscilloscope. None of the chips ceased operation during the tests. After two months to ensure the chips became safe to handle, the two chips (chips 1 and 2) were re-mounted on the test boards and fully retested.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/radiation/nevis10_Supply_Current.pdf}
\end{center}
\caption{Current consumption variation during irradiation. Note the vertical scale. 2500 s corresponds to a dose of 5 MRad.}
\label{fig:current}
\end{figure}

After irradiation the performance measurements were repeated and the results are shown in Table \ref{tab:nevis10results}. The calibration constants, computed through the digital calibration routine, did not change after irradiation. Figure \ref{fig:perf} compares the dynamic ADC performance before and after irradiation as a function of the input signal frequency, showing the radiation-hard nature of the design.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/radiation/nevis10_Pre_Post_comp.pdf}
\end{center}
\caption{Dynamic performance before and after the irradiation (5 MRad) (Chip 1).}
\label{fig:perf}
\end{figure}

\subsection{TID and SEE tests with Nevis12}
\label{sec:nevis12}
The goals of the irradiation test  \cite{Kuppambatti:2017doe} were to measure the SEE cross section and to test the radiation tolerance of the SAR ADC. Five chips were taken to the Massachusetts General Hospital Francis H. Burr Proton Therapy Center. These chips were selected using a socketed board to verify their functionality, and were then mounted on a specialized board designed to allow the readout of the chip during the irradiation. As shown in Figure \ref{fig:board}, the chip was placed at one end of the $\sim30$~cm long board, while the other active elements, including the FGPA, were placed at the other end. The high radiation flux in the ATLAS detector was replicated by irradiating four of the chips using the 227 MeV proton beam. During the irradiation, the active elements not under test were protected with lead shielding. This board was powered and the current drawn by the chip was monitored. A clock signal was provided, and a pure sine-wave signal was applied to the input of the ADC. The board limited the precision of the measurements to approximately 10 ENOB. The limitation prevented the testing of the full 12-bit resolution. However, previous radiation testing of the MDAC stages established that doses as large as 10 MRad did not measurably degrade the MDAC precision.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.8\textwidth]{figures/radiation/Radtestingboard.pdf}
\end{center}
\caption{Test board for irradiation. The length of the board is $\sim30$~cm in order to separate the active elements from the ADC under test. The active elements are protected by lead shielding during testing.}
\label{fig:board}
\end{figure}

\begin{table}[!htb]
%\begin{scriptsize}
\begin{center}
\begin{tabular}{|ccccc|}
\hline
Chip Number & Dose [MRad] & SNDR [dBc] & SFDR [dBc] & ENOB \\
& & Pre/Post-Irradiation & Pre/Post-Irradiation & Pre/Post-Irradiation \\ 
\hline
1 & 2 & 62.43/61.05 & 67.27/70.06 & 10.08/9.85 \\
2 & 1 & 63.54/62.36 & 70.92/72.98 & 10.26/10.10 \\
\hline
\end{tabular}
\caption{Measurements of ADC performance before and immediately after irradiation in a 227 MeV proton beam at $f_{\textrm{in}}=10$~MHz. The change in the ENOB is within the measurement errors of the testing setup.}
\label{tab:nevis12results}
\end{center}
%\end{scriptsize}
\end{table}


The performance of the chip over a large TID was measured by irradiating two chips, one to 1 MRad and the other to 2 MRad. During the irradiation, the ENOB of two of the chips was measured every five seconds, while the calibration was performed every 120 s. The limited precision was sufficient to determine the 8-bit SAR ADC sensitivity to TID and the SEE cross section. The limited precision also enters the measurement through the calibration. Since the calibration constants are measured with this board, they have a reduced precision as well. This resulted in fluctuations in the ENOB after each new calibration was applied, as observed in Figure \ref{fig:enob}. The test demonstrates that the ADC is functioning well after irradiation with up to 2 MRad with no sign of failures and or large degradation of ENOB performance: in future testing it may be possible to improve the precision of the test board. Table \ref{tab:nevis12results} lists the dose for the two chips and the performance measurements before the start of the irradiation and immediately afterwards, confirming the insensitivity of the 8-bit SAR ADC to TID. The calibration constants varied by 1-2\%.

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.7\textwidth]{figures/radiation/nevis12_FFT_enobPlot_zoom.pdf}
\end{center}
\caption{ENOB monitored during irrradiation. The fluctuations in the ENOB are expected and caused by rerunning the calibration procedure several times during the irradiation, as would be done in the use of the device at the ATLAS experiment. Note that on the specialized irradiation board the ENOB is limited to approximately 10. 1100 s corresponds to a dose of 2 MRad, or 75 kRad (the TID specification) in less than 50 s.}
\label{fig:enob}
\end{figure}

Two additional chips were irradiated to measure the SEE cross section. In this test the sine-wave signal was removed. Outputs within a window around the pedestal value were removed by a filter applied in the FPGA. In this way single-event effects could be observed as the appearance of a digital output which was outside the filter window, with very little dead time. Additionally, spurious codes (caused by the clock duty cycle issue) which had been observed before irradiation were masked offline.

Table \ref{tab:nevis12see} lists the dose and cross section measurements for the two chips. There are three types of SEE observed. The first is a single-event-function-interrupt (SEFI), in which the ADC ceases to operate normally. When detected (constant ADC output is observed) an external reset returns the ADC to normal operation. This is not equivalent to a latchup: restoring operation after a latchup would require power cycling the ADC, therefore it can be concluded that latchup events were not observed. The next is a single-event-upset (SEU), which results in a transient, erroneous code. This can affect either the analog or digital portion of the chip. When the analog signal path is affected the errors can be observed as a distribution around the pedestal value, effectively reducing the resolution of the ADC. As a result of the binary scaling of the capacitors in the SAR, the sensitivity to upsets drops quickly with bit significance. That is, the SEU cross section, in this case, decreases as the magnitude of the induced error in the number of ADC counts increases. If the errors result from the digital portion of the chip (bit-flips) then the errors are typically large deviations from the pedestal value. The total SEE is the sum of these errors. In determining the SEE cross section we exclude the analog errors, as they are primarily a resolution effect, but also indicate the value obtained if such errors were to be included.

\begin{table}[!htb]
\begin{scriptsize}
\begin{center}
\begin{tabular}{|cccccccc|}
\hline
Chip Number & Rate & Dose & SEFI & SEU & SEU & SEE & Cross-section (w/ analog errors) \\
& [$10^8$ protons/cm\textsuperscript{2}/s] & [kRad] &  & (Analog) & (Digital) & & [$10^{-12}$ cm\textsuperscript{2}] \\ 
\hline
3 & 19.0 & 101 & 0 & 8 & 1 & 9 & 0.6 ($5.7\pm1.9$)\\
3 & 76.0 & 283 & 0 & 41 & 2 & 43 & 0.6 ($9.8\pm1.5$)\\
4 & 18.6 & 203 & 1 & 10 & 0 & 11 & 0.3 ($3.5\pm1.1$)\\
\hline
\end{tabular}
\caption{Measurements of Nevis12 ADC SEE performance in a 227 MeV proton beam.}
\label{tab:nevis12see}
\end{center}
\end{scriptsize}
\end{table}

\subsection{SEE tests with Nevis14}
Measurements of the Nevis14 ADC SEE cross-sections were performed with a proton beam. The same test setup as the one used for Nevis12 and described in Section \ref{sec:nevis12} was used. A total of two chips were tested and the proton rates varied from 10 to $58\times10^8$ protons/cm\textsuperscript{2}/s. For two runs of one of the chip, the number of observed SEE events and the calculated cross-sections are shown on Table \ref{tab:nevis14see}. The total observed SEE cross-sections are compatible with the values measured for Nevis12.

\begin{table}[!htb]
\begin{scriptsize}
\begin{center}
\begin{tabular}{|ccccccccc|}
\hline
Chip Number & Rate & Dose & SEFI & SEU & SEU & SEE & Cross-section (w/ analog errors) \\
& [$10^8$ protons/cm\textsuperscript{2}/s] & [kRad] & & (Analog) & (Digital) & & [$10^{-12}$ cm\textsuperscript{2}] \\ 
\hline
14 & 11.0 & 50.8 & 1 & 4 & 0 & 5 & 1.1 (5.3)\\
14 & 24.1 & 193.7 & 0 & 19 & 5 & 24 & 1.4 (6.6)\\
%21 & 11.0 & & & & & & & \\ 
\hline
\end{tabular}
\caption{Measurements of the Nevis14 ADC SEE performance in a 227 MeV proton beam.}
\label{tab:nevis14see}
\end{center}
\end{scriptsize}
\end{table}

\subsection{SEE tests with Nevis15}
The goal of the irradiation tests performed with the Nevis15 ADC was to measure the SEE cross-sections using heavy ions, targeting a larger range of deposited energies and complementing the already existing studies with protons.

One chip was taken to the Cyclotron Resource Centre at Louvain-la-Neuve. It was mounted on a specialized board and its lid removed (Figure \ref{fig:board}). The board was then place inside a vacuum chamber and positioned such that the beam of heavy ions was centered in the chip and incident at a perpendicular angle. Four different high-energy heavy ions were used, with characteristcs detailed in Table \ref{tab:hi}. The beam flux ranged from 2 to 15$\times10^{3}$ particles per cm\textsuperscript{2} per s, with a 25 mm diameter.
 
\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.5\textwidth]{figures/radiation/nevis15_louvain_photo.jpg}
\end{center}
\caption{Test board for irradiation.}% The lid of the ADC is removed and the board is placed in a vacuum chamber during testing.}
\label{fig:board}
\end{figure}

\begin{table}[!htb]
%\begin{scriptsize}
\begin{center}
\begin{tabular}{|cccc|}
\hline
Ion & Energy on DUT & Range Si & LET Si \\
& [MeV] & [$\mu$m] & [MeV/(mg/cm\textsuperscript{2})] \\
\hline
\textsuperscript{14}N\textsuperscript{4+} & 122 & 171 & 1.9 \\
\textsuperscript{22}Ne\textsuperscript{7+} & 238 & 202 & 3.3 \\
\textsuperscript{58}Ni\textsuperscript{18+} & 582 & 101 & 20.4 \\
\textsuperscript{124}Xe\textsuperscript{35+} & 995 & 73.1 & 62.5 \\
\hline
\end{tabular}
\caption{High energy heavy ions used for the SEE tests and corresponding energy on device under test (DUT), range and linear energy transfer (LET) in silicon.}
\label{tab:hi}
\end{center}
%\end{scriptsize}
\end{table}

During the test, the chip was powered and clocked at 40 MHz and the current drawn monitored. Similarly to the setup described in Section \ref{sec:nevis12}, a filter was applied in the FPGA such that a single-even-effect is observed as a ADC output code outside the filter window, of $\pm 3$ counts. For the Nickel run, the number and type of events observed on each channel of the chip are shown on Table \ref{tab:run5}, as well as the calculated cross-sections. No latchup events were observed.


The dependence of the SEE rate with the LET of the heavy ion can be parameterized with a Weibull function:
\begin{equation}
\label{eq:weibull}
F(x)=A[1-\exp(-(\frac{x-x_{0}}{W})^{s})]
\end{equation} where $A$ is the plateau cross-section, $x_{0}$ is such that $F(x)=0$ for $x<x_{0}$, $W$ is a width parameter and $s$ is a dimensionless exponent. The total cross-section values measured for all four channels are shown in Figure \ref{fig:LETall} (left), as a function of the linear energy transfer (or heavy ion). A fit to the results of one of the ADC channels is shown on Figure \ref{fig:LETall} (right).

%motivation for weibull function
%rough estimate of proton rate 
\begin{table}[!htb]
%\begin{scriptsize}
\begin{center}
\begin{tabular}{|cccccc|}
\hline
\textsuperscript{58}Ni\textsuperscript{18+}& SEU & SEU & SEFI & SEE & Cross-section (w/ analog errors) \\
& (Analog) & (Digital) & & & [cm$^{2}$] \\ 
\hline
Channel 1 & 59 	& 3  & 1 & 63 & $6.76\times10^{-7}$ ($1.40\times10^{-5}$) \\
Channel 2 & 75 	& 4  & 1 & 80 & $9.01\times10^{-7}$ ($1.78\times10^{-5}$) \\
Channel 3 & 32 	& 2  & 1 & 35 & $4.50\times10^{-7}$ ($7.66\times10^{-6}$) \\
Channel 4 & 61 	& 1  & 1 & 63  & $2.25\times10^{-7}$  ($1.40\times10^{-5}$) \\
\hline
\end{tabular}
\caption{Measurements of the Nevis15 ADC SEE performance in a \textsuperscript{58}Ni\textsuperscript{18+} beam.}
\label{tab:run5}
\end{center}
%\end{scriptsize}
\end{table}

\begin{figure}[htbp!]
\begin{center}
\includegraphics[width=0.47\textwidth]{figures/radiation/XSection_LET_total.pdf}
\includegraphics[width=0.47\textwidth]{figures/radiation/XSection_LET_total_fitChannel4.pdf}
\end{center}
\caption{Total SEE cross-sections for each ADC channel, as a function of LET.}% The lid of the ADC is removed and the board is placed in a vacuum chamber during testing.}
\label{fig:LETall}
\end{figure}


\subsection{Additional TID tests with Nevis15}
An additional proton irradiaton run was performed with the Nevis15 ADC. In order to better characterize the spike behavior in the ADC output when the digital voltage ($V_{dd}$) supplied is lower than the nominal 1.2 V, one chip was taken to the Massachussetts General Hospital for a proton test. 

The goal of the test was to establish if the spike rates and their dependence with $V_{dd}$ were impacted by a large radiation dose. Therefore, using a test setup similar to that of Section \ref{sec:nevis12}, 250 kRad were delivered to the chip. The spike rates were measured for a range of $V_{dd}$ values, both before and after the irradiation, for a single channel (as shown in Figure \ref{fig:preVspost}). A dose of 250 kRad was shown to have negligible impact on the spike rates and on the threshold voltage value for which they start to be observable.

\begin{figure}[htbp!]
\begin{center}
%\includegraphics[width=0.5\textwidth]{figures/radiation/Nevis2Oct16056.jpg}
\includegraphics[width=0.5\textwidth]{figures/radiation/channel4_preVspostIrradiation.pdf}
\end{center}
\caption{Rates of spikes on the ADC output as a function of the digital voltage supplied, measured before and after a total ionizing dose of 250 kRad is delivered.}
\label{fig:preVspost}
\end{figure}

\subsection{Summary}
The radiation tolerance of different Nevis ADC designs was established up to $\sim10$~MRad. Proton SEE cross-sections were measured to be of the order of 10\textsuperscript{-12}~cm\textsuperscript{2} per ADC. The dependence of the SEE cross-sections with the linear energy transfer was measured with heavy ions and observed to be qualitatively as expected. A total dose of 250 kRad was observed to have no impact on the spike behavior observed on the Nevis15 ADC chip for voltages below the nominal operating value.