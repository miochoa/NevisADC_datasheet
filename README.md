## Versions
**datasheet_NevisADC_v4.pdf**
- March 2019
- Adding information on internal o-flag and c-flag (Section 8)

**datasheet_NevisADC_v3.pdf**
- October 2018
- Addressing several comments from people at CPPM.
- Improvements to I2C and calibration information (sections 7 and 8)
- New figure with digital schematics (section 2)
- Added Bibliography

**datasheet_NevisADC_v2.pdf** 
- April 2018